package analyzer;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.ngram.NGramTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.util.Version;

import classification.Configuration;
import db.DBHandler;

public class CustomAnalyzerForQuestionCategory extends StopwordAnalyzerBase {
	static List<String> customStopWords = new ArrayList<>();

	// final static CharArraySet customStopSet = new
	// CharArraySet(Version.LUCENE_48, customStopWords, false);
	// static CharArraySet customStopwordsSet =
	// CharArraySet.unmodifiableSet(customStopSet);

	public CustomAnalyzerForQuestionCategory() {
		// super(Version.LUCENE_48, customStopwordsSet);
		super(Version.LUCENE_48, StandardAnalyzer.STOP_WORDS_SET);

	}

	protected CustomAnalyzerForQuestionCategory(Version version, CharArraySet stopwords) {
		super(version, stopwords);
		// TODO Auto-generated constructor stub
	}

	/** Tokens longer than this length are discarded. Defaults to 50 chars. */
	public int maxTokenLength = 50;

	@Override
	protected TokenStreamComponents createComponents(String fieldName, Reader reader) {

		// final Tokenizer source = new StandardTokenizer(matchVersion, reader);
		// source.setMaxTokenLength(maxTokenLength);

		Tokenizer source = new NGramTokenizer(Version.LUCENE_48, reader, Configuration.NGRAM_MIN, Configuration.NGRAM_MAX);

		TokenStream pipeline = source;
		pipeline = new StandardFilter(matchVersion, pipeline);
		pipeline = new StopFilter(matchVersion, pipeline, StandardAnalyzer.STOP_WORDS_SET);

		customStopWords.addAll(DBHandler.getTagsFromDb(0, 15000));
		final CharArraySet customStopSet = new CharArraySet(Version.LUCENE_48, customStopWords, false);
		CharArraySet customStopwordsSet = CharArraySet.unmodifiableSet(customStopSet);

		pipeline = new StopFilter(matchVersion, pipeline, customStopwordsSet);
		pipeline = new PorterStemFilter(pipeline);
		pipeline = new EnglishPossessiveFilter(matchVersion, pipeline);
		pipeline = new ASCIIFoldingFilter(pipeline);
		pipeline = new LowerCaseFilter(matchVersion, pipeline);

		return new TokenStreamComponents(source, pipeline);
	}
}
