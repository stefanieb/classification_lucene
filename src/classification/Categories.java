package classification;

import java.util.Arrays;
import java.util.List;

public class Categories {
	public static final String[] question_category = { "Better Solution...", "Device...", "Error...", "How to...", "Is it possible...", "Version...",
			"What is the problem...", "Why..." };

	public static final String[] overall_category = { "Webkit", "Utility", "User Interface", "Text", "Telephony", "Security", "Other", "OS",
			"Networking", "Media", "Libs/APIs", "Input", "Graphics", "Database", "Application", "Android(General)" };

	public static final String[] problem_category = { "ADB", "APIs/Frameworks", "APK/dex", "ActionBar", "Activity", "Adapter",
			"AlarmManager, Notification", "Android Sources", "Animation", "Composite", "Context", "Database", "Design", "Dialog", "Emulator/Device",
			"Environment", "EventHandling", "External Devices", "File (I/O) + File System", "Form Widgets", "Fragments", "Gestures ", "Google Play",
			"Graphics", "Hardware", "IDE", "Images & Media", "Informations (UserManager, Build, Preference)", "Intent", "Keyboard",
			"Keystore/certificates", "Layout", "Media", "Memory ", "Menu", "Networking", "Notification", "OS", "Parcelable, Serialization",
			"Permissions/Rights", "PlatformIndependence", "PowerManager", "Provider, Account, Content, KeyGuard", "Ressourcen", "SDK (-Manager)",
			"Screen Rotation/Orientation", "Service", "Shared Preferences", "Telephony", "Text", "Threading/Asynchronous Communication",
			"Time and Date ", "Toast", "Transitions", "Utility", "View", "Webkit", "kein Bezug zu Programmieren/API", };

	public static final String oc = "overall_category";
	public static final String pc = "problem_category";
	public static final String qc = "question_category";

	public static int getIndex(String category) {

		int index = -1;

		while (index == -1) {

			index = findString(overall_category, category);
			if (index != -1) {
				break;
			}
			index = findString(problem_category, category);
			if (index != -1) {
				break;
			}
			index = findString(question_category, category);

		}
		return index;
	}

	private static int findString(String[] categories, String searchCategory) {

		List<String> list = Arrays.asList(categories);

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(searchCategory)) {
				return i;
			}
		}

		return -1;
	}
}
