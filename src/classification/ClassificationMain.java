package classification;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.classification.ClassificationResult;
import org.apache.lucene.classification.Classifier;
import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.SlowCompositeReaderWrapper;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.tartarus.snowball.ext.EnglishStemmer;

import confusionmatrix.ConfusionMatrix;
import crossvalidation.CrossValidation10Fold;
import index.Index;
import model.Result;
import model.StackOverflowPost;

public class ClassificationMain {

	private static Analyzer manalyzer;

	private static String category;

	private List<String> categories;

	private static int OVERALL_sum_count_right = 0;

	private static int ITERATIONS_INCREASING_K = 1;

	private static List<Result> results = new ArrayList<Result>();

	private void init() {
		categories = new ArrayList<>();
		// categories.add("overall_category");
		// categories.add("problem_category");
		categories.add("question_category");
	}

	/**
	 * MAIN
	 */
	public static void main(String[] args) throws IOException, ParseException {
		// 0. Specify the analyzer for tokenizing text.
		// The same analyzer should be used for indexing and searching

		ClassificationMain cm = new ClassificationMain();
		for (int i = 0; i < ITERATIONS_INCREASING_K; i++) {
			cm.writeKToCSV();
			cm.startClassification();
			Configuration.increaseK();
			System.out.println("FINISHED ALL \n RIGHT=" + OVERALL_sum_count_right + " of " + (1150 * Configuration.ITERATIONS_CROSS_VALIDATION));
			OVERALL_sum_count_right = 0;
		}

	}

	private void startClassification() throws IOException {
		init();
		for (int i = 0; i < Configuration.ITERATIONS_CROSS_VALIDATION; i++) {
			startTrainingAndTest();
		}

		ConfusionMatrix.printMatrices();
		ConfusionMatrix.printMatricesToCSV();
	}

	private void startTrainingAndTest() throws IOException {

		CrossValidation10Fold kFoldCrossValidation = new CrossValidation10Fold();

		while (kFoldCrossValidation.hasNextPosts()) {

			List<List<StackOverflowPost>> allPosts = kFoldCrossValidation.nextPosts();

			List<StackOverflowPost> trainingsSet = allPosts.get(0);
			List<StackOverflowPost> testSet = allPosts.get(1);

			for (String cat : categories) {

				int count_right = 0;
				int count_wrong = 0;
				int count_not_classified = 0;

				category = cat;

				// SET ANALYZER

				manalyzer = Configuration.getAnalyzerForCategory(cat);
				// 1. create the index

				/**
				 * TRAINING
				 */

				Index.createIndexForCategory(trainingsSet, Configuration.PATH_TO_INDEX, cat);

				Directory fsDir = FSDirectory.open(new File(Configuration.PATH_TO_INDEX));
				AtomicReader reader = SlowCompositeReaderWrapper.wrap(DirectoryReader.open(fsDir));

				// MORE LIKE THIS

				MoreLikeThis moreLikeThis = new MoreLikeThis(reader);
				moreLikeThis.setAnalyzer(manalyzer);
				moreLikeThis.setFieldNames(new String[] { "tags", "title", "body" });

				// Query query =moreLikeThis.like(docNum)

				// KNEAREST NEIGHBOUR
				CustomKNearestNeighborClassifier classifier = new CustomKNearestNeighborClassifier(Configuration.K);

				// NAIVE BAYES
				// Classifier<BytesRef> classifier = new
				// SimpleNaiveBayesClassifier();
				// classifierNB.train(atomicReader, textFieldNames,
				// classFieldName, analyzer, query);

				classifier.train(reader, new String[] { "body", "tag0", "tag1", "tag2", "tag3", "tag4", "title" }, category, manalyzer, null);

				// System.out.println("TRAINING FINISHED");

				/**
				 * TEST
				 */

				for (StackOverflowPost post : testSet) {
					try {
						if (classifyPost(classifier, post)) {
							count_right++;
						} else {
							count_wrong++;
						}
					} catch (IllegalArgumentException e) {
						count_not_classified++;
					}
				}
				System.out.println("CATEGORY " + category);
				System.out.println("RIGHT: " + count_right);
				System.out.println("WRONG: " + count_wrong);
				System.out.println("NOT CLASSIFIED: " + count_not_classified);

				results.add(new Result(count_right, count_wrong, count_not_classified, cat));

			}

		}
		writeResultsAndSummaryToCSV(results);

		results = new ArrayList<Result>();
		System.out.println("NEXT CROSS VALIDATION");

	}

	private void writeResultsAndSummaryToCSV(List<Result> results) {
		ResultsToCSV.writeResultsToCSV(results);
		ResultsToCSV.writeSummaryToCSV(getResultsListToHashMap(results));

	}

	private Map<String, List<Result>> getResultsListToHashMap(List<Result> results) {

		Map<String, List<Result>> map = new HashMap<>();
		map.put("overall_category", new ArrayList<Result>());
		map.put("problem_category", new ArrayList<Result>());
		map.put("question_category", new ArrayList<Result>());

		for (Result res : results) {
			map.get(res.getCategory()).add(res);
		}

		return map;
	}

	private Boolean classifyPost(CustomKNearestNeighborClassifier classifier, StackOverflowPost post) throws IOException, IllegalArgumentException {

		String body = post.getBody();
		// preprocessing text // TODO

		body = preprocessinBody(body);

		if (category.equals(Categories.qc)) {
			post = preprocessForQuestionCategory(post);
		}

		ClassificationResult<BytesRef> result = classifier.assignClassForOverallCategory(post.getTitle(), post.getTags(), post.getBody());
		// ClassificationResult<BytesRef> result =
		// classifier.assignClass(post.getBody());

		double score = result.getScore();
		if (score >= Configuration.SCORE) {

			String classified = result.getAssignedClass().utf8ToString();

			// get manually assigned class
			String cat_assigned = "";

			if (category.equals("question_category")) {
				cat_assigned = post.getQuestionCategory();
			} else if (category.equals("problem_category")) {
				cat_assigned = post.getProblemCategory();
			} else if (category.equals("overall_category")) {
				cat_assigned = post.getOverallCategory();
			}
			if (!classified.equals(""))
				ConfusionMatrix.fillConfusionMatrix(category, cat_assigned, classified, classified.equals(cat_assigned));

			if (classified.equals(cat_assigned)) {

				OVERALL_sum_count_right++;
				return true;
			} else {

				return false;
			}

		} else {
			// sollte nicht mehr aufgerufen werden
			throw new IllegalArgumentException("Score falsch gesetzt??");
		}

	}

	private Boolean classifyPost(Classifier<BytesRef> classifier, StackOverflowPost post) throws IOException, IllegalArgumentException {

		String body = post.getBody();
		// preprocessing text // TODO

		body = preprocessinBody(body);

		// ClassificationResult<BytesRef> result =
		// classifier.assignClassForOverallCategory(post.getTitle(),
		// post.getTags(), post.getBody());
		ClassificationResult<BytesRef> result = classifier.assignClass(post.getBody());

		double score = result.getScore();
		if (score >= Configuration.SCORE) {

			String classified = result.getAssignedClass().utf8ToString();

			// get manually assigned class
			String cat_assigned = "";

			if (category.equals("question_category")) {
				cat_assigned = post.getQuestionCategory();
			} else if (category.equals("problem_category")) {
				cat_assigned = post.getProblemCategory();
			} else if (category.equals("overall_category")) {
				cat_assigned = post.getOverallCategory();
			}
			if (!classified.equals(""))
				ConfusionMatrix.fillConfusionMatrix(category, cat_assigned, classified, classified.equals(cat_assigned));

			if (classified.equals(cat_assigned)) {

				OVERALL_sum_count_right++;
				return true;
			} else {

				return false;
			}

		} else {
			// sollte nicht mehr aufgerufen werden
			throw new IllegalArgumentException("Score falsch gesetzt??");
		}

	}

	private String preprocessinBody(String text) {

		final List<String> stopWords = Arrays.asList("a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "have", "I", "I'd", "I'm", "if",
				"in", "into", "is", "it", "no", "not", "of", "on", "or", "please", "such", "that", "the", "their", "then", "there", "these", "they",
				"this", "to", "very", "was", "will", "with");

		for (String stopWord : stopWords) {
			text = text.replace(" " + stopWord + " ", " ");
		}

		EnglishStemmer english = new EnglishStemmer();

		english.setCurrent(text);
		english.stem();
		return english.getCurrent();

	}

	private StackOverflowPost preprocessForQuestionCategory(StackOverflowPost post) {
		post.setTags("");
		return post;
	}

	private void writeKToCSV() {
		try {
			FileWriter writer = new FileWriter(Configuration.PATH_TO_FILE + ".csv", true);

			writer.append("\n");
			writer.append("K=" + Configuration.K);
			writer.append("\n");

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
