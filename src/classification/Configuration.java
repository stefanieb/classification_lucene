package classification;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.ngram.NGramTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.util.Version;

import analyzer.CustomAnalyzer;
import analyzer.CustomAnalyzerForQuestionCategory;

public class Configuration {

	public static final Version VERSION = Version.LUCENE_48;

	public static final int NGRAM_MIN = 4;
	public static final int NGRAM_MAX = 4;

	public static final String PATH_TO_INDEX = "/Users/stefanie/Documents/workspace/lucene_index";

	public static final String PATH_TO_FILE = "/Users/stefanie/Documents/workspace/evaluation_juliii";

	public static final String ANALYZER = "cust";
	// private static final String ANALYZER = "std"; // StandardAnalyzer
	// private static final String ANALYZER = "lc"; // LowerCaseFilter
	// private static final String ANALYZER = "ngram"; // NGramTokenizer
	// private static final String ANALYZER = "simple"; // KeyWordTokenizer

	public static final String CLASSIFIER = "kNN";

	public static String[] CATEGORIES = new String[] { "overall_category", "problem_category", "question_category" };
	public static int K = 30; // OC 17-20, QC 30, PC 15

	public static final double SCORE = 0.0;

	public static final int ITERATIONS_CROSS_VALIDATION = 1;

	public static Analyzer getAnalyzerForCategory(String category) {

		Analyzer analyzer;
		// mType = classificationType.toLowerCase();
		if (!("std".equals(ANALYZER) || "lc".equals(ANALYZER) || "ngram".equals(ANALYZER) || "simple".equals(ANALYZER) || "cust".equals(ANALYZER)))
			throw new IllegalArgumentException("unknown analyzer: " + ANALYZER);

		if (category.equals(Categories.qc)) {
			analyzer = new CustomAnalyzerForQuestionCategory();
		} else if ("std".equals(ANALYZER)) {
			analyzer = new StandardAnalyzer(VERSION);
		} else if ("lc".equals(ANALYZER)) {
			analyzer = new Analyzer() {
				@Override
				protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
					Tokenizer source = new StandardTokenizer(VERSION, reader);
					TokenStream filter = new LowerCaseFilter(VERSION, source);
					return new TokenStreamComponents(source, filter);
				}
			};
		} else if ("ngram".equals(ANALYZER)) {
			analyzer = new Analyzer() {
				@Override
				protected TokenStreamComponents createComponents(String fieldName, Reader reader) {

					Tokenizer source = new NGramTokenizer(VERSION, reader, NGRAM_MIN, NGRAM_MAX);

					return new TokenStreamComponents(source);
				}
			};
		} else if ("simple".equals(ANALYZER)) {

			analyzer = new Analyzer() {
				@Override
				protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
					Tokenizer source = new NGramTokenizer(VERSION, reader, NGRAM_MIN, NGRAM_MIN);
					TokenStream filter = new LowerCaseFilter(VERSION, source);
					return new TokenStreamComponents(source, filter);
				}
			};

		} else if ("cust".equals(ANALYZER)) {

			analyzer = new CustomAnalyzer();

		} else {
			analyzer = null;
		}

		return analyzer;
	}

	public static void increaseK() {
		K++;
	}

	public static String[] getTags(String tags) {
		tags = tags.replace("><", ",");
		tags = tags.replace("<", "");
		tags = tags.replace(">", "");

		return tags.split(",");
	}

}
