package classification;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.classification.ClassificationResult;
import org.apache.lucene.classification.Classifier;
import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.BytesRef;

/**
 * A k-Nearest Neighbor classifier (see
 * <code>http://en.wikipedia.org/wiki/K-nearest_neighbors</code>) based on
 * {@link MoreLikeThis}
 * 
 * @lucene.experimental
 */
public class CustomKNearestNeighborClassifier implements Classifier<BytesRef> {

	private MoreLikeThis mlt;
	private String[] textFieldNames;
	private String classFieldName;
	private IndexSearcher indexSearcher;
	private final int k;
	private Query query;

	private int minDocsFreq;
	private int minTermFreq;

	/**
	 * Create a {@link Classifier} using kNN algorithm
	 * 
	 * @param k
	 *            the number of neighbors to analyze as an <code>int</code>
	 */
	public CustomKNearestNeighborClassifier(int k) {
		this.k = k;
	}

	/**
	 * Create a {@link Classifier} using kNN algorithm
	 * 
	 * @param k
	 *            the number of neighbors to analyze as an <code>int</code>
	 * @param minDocsFreq
	 *            the minimum number of docs frequency for MLT to be set with
	 *            {@link MoreLikeThis#setMinDocFreq(int)}
	 * @param minTermFreq
	 *            the minimum number of term frequency for MLT to be set with
	 *            {@link MoreLikeThis#setMinTermFreq(int)}
	 */
	public CustomKNearestNeighborClassifier(int k, int minDocsFreq, int minTermFreq) {
		this.k = k;
		this.minDocsFreq = minDocsFreq;
		this.minTermFreq = minTermFreq;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClassificationResult<BytesRef> assignClass(String text) throws IOException {
		if (mlt == null) {
			throw new IOException("You must first call Classifier#train");
		}
		BooleanQuery mltQuery = new BooleanQuery();

		// TODO für jedes feld eigene clause

		for (String textFieldName : textFieldNames) {
			mltQuery.add(new BooleanClause(mlt.like(new StringReader(text), textFieldName), BooleanClause.Occur.SHOULD));
		}
		Query classFieldQuery = new WildcardQuery(new Term(classFieldName, "*"));
		mltQuery.add(new BooleanClause(classFieldQuery, BooleanClause.Occur.MUST));
		if (query != null) {
			mltQuery.add(query, BooleanClause.Occur.MUST);
		}
		TopDocs topDocs = indexSearcher.search(mltQuery, k);
		return selectClassFromNeighbors(topDocs);
	}

	public ClassificationResult<BytesRef> assignClassForOverallCategory(String title, String tags, String body) throws IOException {

		if (mlt == null) {
			throw new IOException("You must first call Classifier#train");
		}

		BooleanQuery mltQuery = new BooleanQuery();

		// wo min 1 tag übereinstimmt

		if (!tags.equals("")) {
			Query tagquery;
			BooleanQuery tagQueries = new BooleanQuery();
			tagQueries.setMinimumNumberShouldMatch(1);
			mltQuery.add(tagQueries, Occur.MUST);

			for (String tag : Configuration.getTags(tags)) {
				for (String textFieldName : textFieldNames) {

					if (textFieldName.startsWith("tag")) {
						tagquery = new TermQuery(new Term(textFieldName, tag));
						tagquery.setBoost(1.5f);
						tagQueries.add(tagquery, Occur.SHOULD);
					} else {
						BooleanClause booleanClause = new BooleanClause(mlt.like(new StringReader(tag), textFieldName), BooleanClause.Occur.SHOULD);
						mltQuery.add(booleanClause);
					}

					// TODO
					// mlt.setStopWords(stopWords);
				}
			}
		}

		// "body", "tag1", "tag2", "tag3", "tag4", "tag5", "title"
		// TODO für jedes feld eigene clause

		//

		// BODY

		mltQuery.add(new BooleanClause(mlt.like(new StringReader(body), textFieldNames[0]), BooleanClause.Occur.SHOULD));
		// mltQuery.add(new BooleanClause(mlt.like(new StringReader(body),
		// textFieldNames[6]), BooleanClause.Occur.SHOULD));

		// TITLE
		// mltQuery.add(new BooleanClause(mlt.like(new StringReader(title),
		// textFieldNames[0]), BooleanClause.Occur.SHOULD));
		// mltQuery.add(new BooleanClause(mlt.like(new StringReader(title),
		// textFieldNames[6]), BooleanClause.Occur.SHOULD));

		Query classFieldQuery = new WildcardQuery(new Term(classFieldName, "*"));
		mltQuery.add(new BooleanClause(classFieldQuery, BooleanClause.Occur.MUST));
		if (query != null) {
			mltQuery.add(query, BooleanClause.Occur.MUST);
		}
		TopDocs topDocs = indexSearcher.search(mltQuery, k);
		return selectClassFromNeighbors(topDocs);
	}

	private ClassificationResult<BytesRef> selectClassFromNeighbors(TopDocs topDocs) throws IOException {
		// TODO : improve the nearest neighbor selection
		Map<BytesRef, Integer> classCounts = new HashMap<>();
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			BytesRef cl = new BytesRef(indexSearcher.doc(scoreDoc.doc).getField(classFieldName).stringValue());
			Integer count = classCounts.get(cl);
			if (count != null) {
				classCounts.put(cl, count + 1);
			} else {
				classCounts.put(cl, 1);
			}
		}
		double max = 0;
		BytesRef assignedClass = new BytesRef();
		for (Map.Entry<BytesRef, Integer> entry : classCounts.entrySet()) {
			Integer count = entry.getValue();
			if (count > max) {
				max = count;
				assignedClass = entry.getKey().clone();
			}
		}
		double score = max / (double) k;
		return new ClassificationResult<>(assignedClass, score);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void train(AtomicReader atomicReader, String textFieldName, String classFieldName, Analyzer analyzer) throws IOException {
		train(atomicReader, textFieldName, classFieldName, analyzer, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void train(AtomicReader atomicReader, String textFieldName, String classFieldName, Analyzer analyzer, Query query) throws IOException {
		train(atomicReader, new String[] { textFieldName }, classFieldName, analyzer, query);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void train(AtomicReader atomicReader, String[] textFieldNames, String classFieldName, Analyzer analyzer, Query query) throws IOException {
		this.textFieldNames = textFieldNames;
		this.classFieldName = classFieldName;
		mlt = new MoreLikeThis(atomicReader);
		mlt.setAnalyzer(analyzer);
		mlt.setFieldNames(textFieldNames);
		indexSearcher = new IndexSearcher(atomicReader);
		if (minDocsFreq > 0) {
			mlt.setMinDocFreq(minDocsFreq);
		}
		if (minTermFreq > 0) {
			mlt.setMinTermFreq(minTermFreq);
		}
		this.query = query;
	}
}
