package classification;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.FieldType.NumericType;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

public class Index {

	private static Directory index;

	public Index(String pathToIndex) throws IOException {

	}

	public static void indexPosts(List<StackOverflowPost> posts, String cat) {

		try {

			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_48, Configuration.getAnalyzerForCategory(cat));
			config.setSimilarity(new DefaultSimilarity());

			IndexWriter writer = new IndexWriter(index, config);
			for (StackOverflowPost post : posts) {

				addDoc(writer, post.getId(), post.getTitle(), post.getBody(), post.getTags(), post.getQuestionCategory(), post.getProblemCategory(),
						post.getOverallCategory());

			}
			writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void addDoc(IndexWriter w, int id, String title, String body, String tags, String question_category, String problem_category,
			String overall_category) throws IOException {
		Document doc = new Document();

		FieldType idField = new FieldType();
		idField.setStored(true);
		idField.setIndexed(false);
		idField.setTokenized(false);
		idField.setNumericType(NumericType.INT);

		doc.add(new IntField("id", id, idField));

		Field titleField = new TextField("title", title, Field.Store.NO);
		titleField.setBoost(2f);
		doc.add(titleField);

		doc.add(new TextField("body", body, Field.Store.NO));

		Field tagsField = new TextField("tags", tags, Field.Store.NO);
		tagsField.setBoost(5f);
		doc.add(tagsField);

		doc.add(new StringField("question_category", question_category, Field.Store.YES));
		doc.add(new StringField("problem_category", problem_category, Field.Store.YES));
		doc.add(new StringField("overall_category", overall_category, Field.Store.YES));

		w.addDocument(doc);
	}

	public static void createIndex(List<StackOverflowPost> posts, String pathToIndex, String cat) throws IOException {

		File index_directory = new File(pathToIndex);
		index = new SimpleFSDirectory(index_directory);

		// System.out.println("START creating index");

		if (index_directory.listFiles() != null) {
			for (File f : index_directory.listFiles()) {
				f.delete();
			}
		}

		indexPosts(posts, cat);
		// System.out.println("ENDE creating index");

	}
}
