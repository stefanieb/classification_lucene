package classification;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import model.Result;

public class ResultsToCSV {

	public static void writeSummaryToCSV(Map<String, List<Result>> results) {
		try {
			FileWriter writer = new FileWriter(Configuration.PATH_TO_FILE + ".csv", true);

			writer.append("Summary:");

			writer.append(',');
			writer.append("Right");
			writer.append(',');
			writer.append("%");
			writer.append(',');
			writer.append("Wrong");
			writer.append('\n');

			for (int i = 0; i < Configuration.CATEGORIES.length; i++) {

				List<Result> resPerCategory = results.get(Configuration.CATEGORIES[i]);
				Integer sum_right = 0;
				Integer sum_wrong = 0;
				Integer sum_not_classified = 0;
				Integer summ_all = 0;
				for (Result res : resPerCategory) {
					sum_right += res.getCountRight();
					sum_wrong += res.getCountWrong();
					sum_not_classified += res.getCountNotClassified();
					summ_all += res.getCountSumAll();
				}

				writer.append(Configuration.CATEGORIES[i]);
				writer.append(',');
				writer.append(sum_right.toString());
				writer.append(',');
				String percentage = ((Double) (sum_right / (summ_all + 0.0))).toString();
				writer.append(percentage.length() > 5 ? percentage.substring(0, 5) : percentage + "%");
				writer.append(',');
				writer.append(sum_wrong.toString());
				writer.append('\n');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void writeResultsToCSV(List<Result> results) {
		try {

			// 30 results
			FileWriter writer = new FileWriter(Configuration.PATH_TO_FILE + ".csv", true);

			writer.append("Analyzer = " + Configuration.ANALYZER);
			writer.append(',');
			writer.append("Classifier = " + Configuration.CLASSIFIER);
			writer.append(',');

			writer.append("Score = " + results.get(0).getScore().toString());
			writer.append(',');
			writer.append("Iterations = " + results.get(0).getIterations().toString());
			writer.append('\n');

			writer.append("OC (right)");
			writer.append(',');
			writer.append("OC (%)");
			writer.append(',');
			writer.append("OC (wrong)");
			writer.append(',');
			writer.append(',');
			writer.append("PC (right)");
			writer.append(',');
			writer.append("PC (%)");
			writer.append(',');
			writer.append("PC (wrong)");
			writer.append(',');
			writer.append(',');
			writer.append("QC (right)");
			writer.append(',');
			writer.append("QC (%)");
			writer.append(',');
			writer.append("QC (wrong)");

			writer.append(',');
			writer.append('\n');

			// ok, pk, qk
			int count = 0;
			for (Result result : results) {

				writer.append(result.getCountRight().toString());
				writer.append(',');
				int length = result.getAvgRight().toString().length();
				writer.append(length > 4 ? result.getAvgRight().toString().substring(0, 5) : result.getAvgRight().toString() + "%");
				writer.append(',');
				writer.append(result.getCountWrong().toString());
				writer.append(',');
				writer.append(',');

				count++;
				if (count % 3 == 0) {
					writer.append('\n');
				}

			}
			writer.append('\n');
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
