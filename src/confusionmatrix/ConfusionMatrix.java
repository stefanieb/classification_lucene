package confusionmatrix;

import java.io.FileWriter;
import java.io.IOException;

import classification.Categories;

public class ConfusionMatrix {

	/**
	 * observed >
	 * 
	 * predicted v
	 * 
	 */

	private static int[][] confusionMatrix_OC = new int[Categories.overall_category.length][Categories.overall_category.length];
	private static int[][] confusionMatrix_PC = new int[Categories.problem_category.length][Categories.problem_category.length];
	private static int[][] confusionMatrix_QC = new int[Categories.question_category.length][Categories.question_category.length];

	public static void fillConfusionMatrix(String category, String cat_assigned, String classified, boolean equals) {

		if (category.equals(Categories.oc)) {
			fillConfusionMatrixOC(cat_assigned, classified, equals);
		} else if (category.equals(Categories.pc)) {
			fillConfusionMatrixPC(cat_assigned, classified, equals);
		} else if (category.equals(Categories.qc)) {
			fillConfusionMatrixQC(cat_assigned, classified, equals);
		}

	}

	private static void fillConfusionMatrixOC(String cat_assigned, String classified, boolean equals) {
		int observed = Categories.getIndex(cat_assigned);
		int predicted = Categories.getIndex(classified);

		confusionMatrix_OC[observed][predicted]++;

	}

	private static void fillConfusionMatrixPC(String cat_assigned, String classified, boolean equals) {
		int observed = Categories.getIndex(cat_assigned);
		int predicted = Categories.getIndex(classified);

		confusionMatrix_PC[observed][predicted]++;
	}

	private static void fillConfusionMatrixQC(String cat_assigned, String classified, boolean equals) {
		int observed = Categories.getIndex(cat_assigned);
		int predicted = Categories.getIndex(classified);

		confusionMatrix_QC[observed][predicted]++;
	}

	public static void printMatrices() {

		// overallCategories
		System.out.println("Overall Category");
		printMatrix(confusionMatrix_OC);

		// problemCategories
		System.out.println("Problem Category");
		printMatrix(confusionMatrix_PC);

		// questionCategories
		System.out.println("Question Category");
		printMatrix(confusionMatrix_QC);

	}

	public static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (i == j) {
					System.err.printf("| %4d ", matrix[i][j]);
				} else {
					System.out.printf("| %4d ", matrix[i][j]);
				}

			}
			System.out.println("|");
		}
	}

	public static void printMatricesToCSV() {

		// overallCategories
		System.out.println("Overall Category to CSV");
		printMatrixToCSV(confusionMatrix_OC, Categories.overall_category, Categories.oc);

		// problemCategories
		System.out.println("Problem Category to CSV");
		printMatrixToCSV(confusionMatrix_PC, Categories.problem_category, Categories.pc);

		// questionCategories
		System.out.println("Question Category to CSV");
		printMatrixToCSV(confusionMatrix_QC, Categories.question_category, Categories.qc);

	}

	private static void printMatrixToCSV(int[][] matrix, String[] questionCategory, String category) {
		try {
			FileWriter writer = new FileWriter("/Users/stefanie/Documents/workspace/LuceneTextMining/evaluation_matrix_" + category + ".csv", false);

			writer.append(category);
			writer.append(',');

			for (int i = 0; i < questionCategory.length; i++) {
				writer.append(questionCategory[i]);
				writer.append(',');
			}

			writer.append('\n');

			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < matrix.length + 1; j++) {
					if (j == 0) {
						writer.append(questionCategory[i]);
						writer.append(',');
					} else {
						writer.append(Integer.toString(matrix[i][j - 1]));
						writer.append(',');
					}

				}
				writer.append('\n');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
