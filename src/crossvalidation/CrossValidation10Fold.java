package crossvalidation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.StackOverflowPost;
import classification.Categories;
import db.DBHandler;

public class CrossValidation10Fold {

	private final int K = 10;

	private List<List<StackOverflowPost>> folds;

	private int TESTSET = 0;

	public CrossValidation10Fold() {
		initFolds(DBHandler.getPostsFromDb(0, 1150));
	}

	private void initFolds(List<List<StackOverflowPost>> postsFromDb) {
		List<StackOverflowPost> list = postsFromDb.get(0);
		folds = new ArrayList<List<StackOverflowPost>>();

		int foldSize = list.size() / K;

		Set<String> overall_category = new HashSet<String>();
		Set<String> problem_category = new HashSet<String>();
		Set<String> question_category = new HashSet<String>();

		List<StackOverflowPost> foldPosts;
		int counter = 0;
		for (int i = 0; i < K; i++) {
			foldPosts = new ArrayList<StackOverflowPost>();
			for (int j = 0; j < foldSize; j++) {
				// System.out.println(counter);
				StackOverflowPost post = list.get(counter);
				foldPosts.add(post);

				counter++;
				// if (i == 0) {
				// overall_category.add(post.getOverallCategory());
				// problem_category.add(post.getProblemCategory());
				// question_category.add(post.getQuestionCategory());
				// }
			}
			folds.add(foldPosts);
		}

		// if (!checkSanity(overall_category.size(), problem_category.size(),
		// question_category.size())) {
		// initFolds(DBHandler.getPostsFromDb(0, 1150));
		// }

	}

	private boolean checkSanity(int oc_size, int pc_size, int qc_size) {
		if (oc_size == Categories.overall_category.length && pc_size == Categories.problem_category.length
				&& qc_size == Categories.question_category.length) {
			return true;
		}
		return false;
	}

	public List<List<StackOverflowPost>> nextPosts() {

		List<List<StackOverflowPost>> returnList = new ArrayList<List<StackOverflowPost>>();

		List<StackOverflowPost> trainingSet = new ArrayList<StackOverflowPost>();
		List<StackOverflowPost> testSet = new ArrayList<StackOverflowPost>();

		for (int i = 0; i < folds.size(); i++) {
			if (i != TESTSET) {
				trainingSet.addAll(folds.get(i));
			} else {
				testSet.addAll(folds.get(i));
			}
		}

		returnList.add(trainingSet);
		returnList.add(testSet);

		TESTSET++;
		return returnList;

	}

	public boolean hasNextPosts() {
		if (TESTSET < K) {
			return true;
		}
		return false;
	}

	public int getTestSet() {
		return TESTSET;
	}

}
