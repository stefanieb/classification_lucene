package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.StackOverflowPost;

public class DBHandler {

	private static String url = "jdbc:mysql://localhost:3306/";
	private static String dbName = "man_categorization";
	private static String driver = "com.mysql.jdbc.Driver";
	private static String userName = "root";
	private static String password = "";

	public static List<List<StackOverflowPost>> getPostsFromDb(int limit_start, int limit_end) {

		List<List<StackOverflowPost>> returnList = new ArrayList<List<StackOverflowPost>>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet posts_db = st.executeQuery(
					"select post_id as 'Id', Title, Tags, Body, question_category as 'qc', problem_category as 'pc', overall_category as 'oc', post_id from matrix join aposts posts on matrix.post_id = posts.Id order by rand() limit "
							+ limit_start + ", " + limit_end + ";");

			List<StackOverflowPost> posts = new ArrayList<StackOverflowPost>();

			while (posts_db.next()) {

				StackOverflowPost post = new StackOverflowPost(posts_db.getInt("Id"), posts_db.getString("Title"),
						removeHTMLTags(posts_db.getString("Body")), posts_db.getString("Tags"), posts_db.getString("qc"), posts_db.getString("pc"),
						posts_db.getString("oc"));

				posts.add(post);

			}
			conn.close();

			returnList.add(posts);

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnList;

	}

	public static String removeHTMLTags(String body) {
		String codeOfBody = body;

		codeOfBody = codeOfBody.replace("\n", "");

		codeOfBody = codeOfBody.replaceAll("<a (.+?)</a>", "");
		codeOfBody = codeOfBody.replaceAll("<img (.+?)>", "");

		codeOfBody = codeOfBody.replace("&#xA;", " ");

		codeOfBody = codeOfBody.replace("&gt;", ">");
		codeOfBody = codeOfBody.replace("&lt;", "<");
		codeOfBody = codeOfBody.replace("<p>", "");
		codeOfBody = codeOfBody.replace("</p>", "");
		codeOfBody = codeOfBody.replace("<br>", "");
		codeOfBody = codeOfBody.replace("</br>", "");
		codeOfBody = codeOfBody.replace("<br/>", "");

		codeOfBody = codeOfBody.replace("<strong>", "");
		codeOfBody = codeOfBody.replace("</strong>", "");
		codeOfBody = codeOfBody.replace("<blockquote>", "");
		codeOfBody = codeOfBody.replace("</blockquote>", "");
		codeOfBody = codeOfBody.replace("<pre>", "");
		codeOfBody = codeOfBody.replace("</pre>", "");
		codeOfBody = codeOfBody.replace("<em>", "");
		codeOfBody = codeOfBody.replace("</em>", "");
		codeOfBody = codeOfBody.replace("<ul>", "");
		codeOfBody = codeOfBody.replace("</ul>", "");
		codeOfBody = codeOfBody.replace("<li>", "");
		codeOfBody = codeOfBody.replace("</li>", "");
		codeOfBody = codeOfBody.replace("<h1>", "");
		codeOfBody = codeOfBody.replace("</h1>", "");
		codeOfBody = codeOfBody.replace("<h2>", "");
		codeOfBody = codeOfBody.replace("</h2>", "");

		return codeOfBody;
	}

	public static List<String> getTagsFromDb(int limit_start, int limit_end) {

		List<String> returnList = new ArrayList<String>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select distinct tagName from posts join post2tags on posts.Id = post2tags.postId join tags_neu on post2tags.tagId = tags_neu.Id order by tagName asc limit "
							+ limit_start + ", " + limit_end + ";");

			while (tags_db.next()) {

				String tag = tags_db.getString("tagName");
				returnList.add(tag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnList;

	}

}
