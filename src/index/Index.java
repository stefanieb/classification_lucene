package index;

import java.io.File;
import java.io.IOException;
import java.util.List;

import model.StackOverflowPost;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import classification.Categories;
import classification.Configuration;

public abstract class Index {

	private static Directory index_dir;

	public void indexPosts(List<StackOverflowPost> posts, String cat) {

		try {

			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_48, Configuration.getAnalyzerForCategory(cat));
			// TODO??
			config.setSimilarity(new DefaultSimilarity());

			IndexWriter writer = new IndexWriter(index_dir, config);
			for (StackOverflowPost post : posts) {

				this.addDoc(writer, post.getId(), post.getTitle(), post.getBody(), post.getTags(), post.getQuestionCategory(),
						post.getProblemCategory(), post.getOverallCategory());

			}
			writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public abstract void addDoc(IndexWriter w, int id, String title, String body, String tags, String question_category, String problem_category,
			String overall_category) throws IOException;

	public void createIndex(List<StackOverflowPost> posts, String pathToIndex, String category) throws IOException {
		File index_directory = new File(pathToIndex);
		index_dir = new SimpleFSDirectory(index_directory);

		// System.out.println("START creating index");

		if (index_directory.listFiles() != null) {
			for (File f : index_directory.listFiles()) {
				f.delete();
			}
		}

		indexPosts(posts, category);
		// System.out.println("ENDE creating index");

	}

	public static void createIndexForCategory(List<StackOverflowPost> posts, String pathToIndex, String cat) throws IOException {
		Index index = null;
		if (cat.equals(Categories.qc)) {
			index = new IndexForQuestionCategories();
		} else if (cat.equals(Categories.oc)) {
			index = new IndexForOverallCategories();
		} else if (cat.equals(Categories.pc)) {
			index = new IndexForProblemCategories();
		}
		index.createIndex(posts, pathToIndex, cat);

	}
}
