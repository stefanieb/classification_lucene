package index;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.FieldType.NumericType;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;

import classification.Configuration;

public class IndexForProblemCategories extends Index {

	@Override
	public void addDoc(IndexWriter w, int id, String title, String body, String tags, String question_category, String problem_category,
			String overall_category) throws IOException {
		Document doc = new Document();

		// ID
		FieldType idField = new FieldType();
		idField.setStored(true);
		idField.setIndexed(false);
		idField.setTokenized(false);
		idField.setNumericType(NumericType.INT);

		doc.add(new IntField("id", id, idField));

		// TAGs
		String[] alltags = Configuration.getTags(tags);

		for (int i = 0; i < alltags.length; i++) {
			String tag = alltags[i];

			// indexed but not tokenized
			Field tagsField = new StringField("tag" + (i + 1), tag, Field.Store.YES);
			// tagsField.setBoost(5.0f);

			doc.add(tagsField);
		}

		// TITLE
		Field titleField = new TextField("title", title, Field.Store.YES);
		titleField.setBoost(2f);
		doc.add(titleField);

		// BODY
		doc.add(new TextField("body", body, Field.Store.NO));
		titleField.setBoost(calculateBoostWithTags(body.toLowerCase(), alltags));

		doc.add(new StringField("problem_category", problem_category, Field.Store.YES));

		w.addDocument(doc);

	}

	private float calculateBoostWithTags(String text, String[] tags) {
		float boost = 1.0f;
		for (String tag : tags) {
			if (text.contains(tag)) {
				boost++;
			}
		}

		return boost;
	}

}
