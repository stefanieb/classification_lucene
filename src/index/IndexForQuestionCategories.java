package index;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.FieldType.NumericType;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;

import classification.Categories;

public class IndexForQuestionCategories extends Index {

	public IndexForQuestionCategories() throws IOException {

	}

	public void addDoc(IndexWriter w, int id, String title, String body, String tags, String question_category, String problem_category,
			String overall_category) throws IOException {
		Document doc = new Document();

		// ID
		FieldType idField = new FieldType();
		idField.setStored(true);
		idField.setIndexed(false);
		idField.setTokenized(false);
		idField.setNumericType(NumericType.INT);

		doc.add(new IntField("id", id, idField));

		// TITLE
		Field titleField = new TextField("title", title, Field.Store.YES);
		titleField.setBoost(calculateBoost(title.toLowerCase(), question_category));
		doc.add(titleField);

		// BODY
		doc.add(new TextField("body", body, Field.Store.NO));
		titleField.setBoost(calculateBoost(body.toLowerCase(), question_category));

		// CATEGORY
		doc.add(new StringField("question_category", question_category, Field.Store.YES));

		w.addDocument(doc);
	}

	private float calculateBoost(String s, String question_category) {

		// Better solution
		if (question_category.equals(Categories.question_category[0])) {
			return calculateBetterSolutionBoostFactor(s);
		}
		// Device
		else if (question_category.equals(Categories.question_category[1])) {
			return calculateDeviceBoostFactor(s);
		}
		// Error
		else if (question_category.equals(Categories.question_category[2])) {
			return calculateErrorBoostFactor(s);
		}
		// How to
		else if (question_category.equals(Categories.question_category[3])) {
			return calculateHowToBoostFactor(s);
		}
		// Is it possible
		else if (question_category.equals(Categories.question_category[4])) {
			return calculateIsItPossibleBoostFactor(s);
		}
		// Version
		else if (question_category.equals(Categories.question_category[5])) {
			return calculateVersionBoostFactor(s);
		} // Problem
		else if (question_category.equals(Categories.question_category[6])) {
			return calculateProblemBoostFactor(s);

		} // Why
		else if (question_category.equals(Categories.question_category[7])) {
			return calculateWhyBoostFactor(s);
		}
		return 0.0f;

	}

	/**
	 * BETTER SOLUTION
	 * 
	 * @param s
	 * @return
	 */
	private float calculateBetterSolutionBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("better solution")) {
			boostFactor += 2.0;
		} else if (s.contains("better way")) {
			boostFactor += 2.0;
		} else if (s.contains("other possibility")) {
			boostFactor += 2.0;
		} else if (s.contains("best practice")) {
			boostFactor += 2.0;
		} else if (s.contains("other way")) {
			boostFactor += 2.0;
		} else if (s.contains("workaround")) {
			boostFactor += 1.5;
		}
		return boostFactor;
	}

	/**
	 * DEVICE
	 * 
	 * @param s
	 * @return
	 */
	private float calculateDeviceBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("device")) {
			boostFactor += 3.5;
		} else if (s.contains("htc")) {
			boostFactor += 1.5;
		} else if (s.contains("lg")) {
			boostFactor += 1.5;
		} else if (s.contains("nexus")) {
			boostFactor += 1.5;
		} else if (s.contains("samsung")) {

		}
		return boostFactor;
	}

	/**
	 * ERROR
	 * 
	 * @param s
	 * @return
	 */
	private float calculateErrorBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("exception")) {
			boostFactor += 3.5;
		} else if (s.contains("stack trace")) {
			boostFactor += 1.5;
		} else if (s.contains("error")) {
			boostFactor += 2.5;
		} else if (s.contains("failure")) {
			boostFactor += 2.5;
		} else if (s.contains("runtimeexception")) {
			boostFactor += 3.5;
		} else if (s.contains("nullpointerexception")) {
			boostFactor += 3.5;
		} else if (s.contains("logcat")) {
			boostFactor += 1.5;
		}
		return boostFactor;
	}

	/**
	 * HOW TO
	 * 
	 * @param s
	 * @return
	 */
	private float calculateHowToBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("how to")) {
			boostFactor += 2.5;
		} else if (s.contains("how do")) {
			boostFactor += 2.0;
			// } else if (s.contains("")) {
			// boostFactor += 0.2;
			// } else if (s.contains("background")) {
			// boostFactor += 0.2;
			// } else if (s.contains("why")) {

		}
		return boostFactor;
	}

	/**
	 * POSSIBLE
	 * 
	 * @param s
	 * @return
	 */
	private float calculateIsItPossibleBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("is it possible")) {
			boostFactor += 3.5;
		} else if (s.contains("possible")) {
			boostFactor += 2.0;
		} else if (s.contains("is there")) {
			boostFactor += 1.5;
		} else if (s.contains("is there")) {
			boostFactor += 1.5;
			// } else if (s.contains("why")) {

		}
		return boostFactor;
	}

	/**
	 * VERSION
	 * 
	 * @param s
	 * @return
	 */
	private float calculateVersionBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("version")) {
			boostFactor += 3.5;
		}
		// TODO suche namen von versionen
		else if (s.contains("ics")) {
			boostFactor += 2.5;
		} else if (s.contains("ice cream sandwich")) {
			boostFactor += 2.5;
		} else if (s.contains("release")) {
			boostFactor += 1.5;
		} else if (s.contains("upgrade")) {
			boostFactor += 1.5;
		}
		return boostFactor;
	}

	/**
	 * PROBLEM
	 * 
	 * @param s
	 * @return
	 */
	private float calculateProblemBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("problem")) {
			boostFactor += 2.5;
		} else if (s.contains("not working")) {
			boostFactor += 2.5;
		} else if (s.contains("my solution")) {
			boostFactor += 1.5;
		} else if (s.contains("error")) {
			boostFactor -= 1.5;
		} else if (s.contains("why")) {
			//
		}
		return boostFactor;
	}

	/**
	 * WHY
	 * 
	 * @param s
	 * @return
	 */
	private float calculateWhyBoostFactor(String s) {
		float boostFactor = 1.0f;
		if (s.contains("why")) {
			boostFactor += 3.0;
		} else if (s.contains("reason")) {
			boostFactor += 2.5;
		} else if (s.contains("opinion")) {
			boostFactor += 2.5;
		} else if (s.contains("background")) {
			boostFactor += 2.0;
		} else if (s.contains("difference")) {
			boostFactor += 2.0;
		}
		return boostFactor;
	}
}
