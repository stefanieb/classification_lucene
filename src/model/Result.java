package model;

import classification.Configuration;

public class Result {

	private Integer k = Configuration.K;
	private Double score = Configuration.SCORE;
	private Integer iterations = Configuration.ITERATIONS_CROSS_VALIDATION;

	private Integer countRight;
	private Integer countWrong;
	private Integer countNotClassified;
	private Integer countSumAll;
	private String category;
	private Double avgRight;

	public Result(Integer count_right, Integer count_wrong, Integer count_not_classified, String category) {
		super();
		this.countRight = count_right;
		this.countWrong = count_wrong;
		this.countNotClassified = count_not_classified;
		this.category = category;
		this.countSumAll = countRight + countWrong + countNotClassified;
		this.avgRight = countRight / (countSumAll + 0.0);
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Integer getIterations() {
		return iterations;
	}

	public void setIterations(Integer iterations) {
		this.iterations = iterations;
	}

	public Integer getCountRight() {
		return countRight;
	}

	public void setCountRight(Integer countRight) {
		this.countRight = countRight;
	}

	public Integer getCountWrong() {
		return countWrong;
	}

	public void setCountWrong(Integer countWrong) {
		this.countWrong = countWrong;
	}

	public Integer getCountNotClassified() {
		return countNotClassified;
	}

	public void setCountNotClassified(Integer countNotClassified) {
		this.countNotClassified = countNotClassified;
	}

	public Integer getCountSumAll() {
		return countSumAll;
	}

	public void setCountSumAll(Integer countSumAll) {
		this.countSumAll = countSumAll;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Double getAvgRight() {
		return avgRight;
	}

	public void setAvgRight(Double avgRight) {
		this.avgRight = avgRight;
	}

}
