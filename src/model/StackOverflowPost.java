package model;

public class StackOverflowPost {

	private Integer id;
	private String title;
	private String body;
	private String tags;
	private String questionCategory;
	private String problemCategory;
	private String overallCategory;

	public StackOverflowPost(int id, String title, String body, String tags, String question_category, String problem_category,
			String overall_category) {
		super();
		this.id = id;
		this.title = title;
		this.body = body;
		this.tags = tags;
		this.questionCategory = question_category;
		this.problemCategory = problem_category;
		this.overallCategory = overall_category;
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getQuestionCategory() {
		return questionCategory;
	}

	public void setQuestionCategory(String question_category) {
		this.questionCategory = question_category;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problem_category) {
		this.problemCategory = problem_category;
	}

	public String getOverallCategory() {
		return overallCategory;
	}

	public void setOverallCategory(String overall_category) {
		this.overallCategory = overall_category;
	}

}
