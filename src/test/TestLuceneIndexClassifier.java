package test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.classification.ClassificationResult;
import org.apache.lucene.classification.SimpleNaiveBayesClassifier;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.SlowCompositeReaderWrapper;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

public final class TestLuceneIndexClassifier {

	public static final String INDEX = "/Users/stefanie/Documents/workspace/LuceneTextMining/lucene_index";
	public static final String[] CATEGORIES = { "Error/Exception/Crash (also compiler errors)", "Where/What is the problem",
			"Why?/Reason?/Understanding?/Difference/Explanation/Opinion", "How To.../Example/Solution",
			"Better Solution/Best Practice/ Is it correct.../Workaround/Simpler Way", "Version Change/Compatibility of APIs",
			"Is it possible...?/ Is there...? /What is...?", "Device" };
	private static int[][] counts;
	private static Map<String, Integer> catindex;

	public static void main(String[] args) throws Exception {
		init();

		final long startTime = System.currentTimeMillis();
		SimpleNaiveBayesClassifier classifier = new SimpleNaiveBayesClassifier();
		IndexReader reader = DirectoryReader.open(dir());
		AtomicReader ar = SlowCompositeReaderWrapper.wrap(reader);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_48);
		classifier.train(ar, "body", "question_category", analyzer);
		final int maxdoc = reader.maxDoc();
		for (int i = 0; i < maxdoc; i++) {
			Document doc = ar.document(i);
			String correctAnswer = doc.get("question_category");
			final int cai = idx(correctAnswer);
			ClassificationResult<BytesRef> result = classifier.assignClass(doc.get("body"));
			String classified = result.getAssignedClass().utf8ToString();
			final int cli = idx(classified);
			counts[cai][cli]++;
		}
		final long endTime = System.currentTimeMillis();
		final int elapse = (int) (endTime - startTime) / 1000;

		// print results
		int fc = 0, tc = 0;
		for (int i = 0; i < CATEGORIES.length; i++) {
			for (int j = 0; j < CATEGORIES.length; j++) {
				System.out.printf(" %3d ", counts[i][j]);
				if (i == j) {
					tc += counts[i][j];
				} else {
					fc += counts[i][j];
				}
			}
			System.out.println();
		}
		float accrate = (float) tc / (float) (tc + fc);
		float errrate = (float) fc / (float) (tc + fc);
		System.out.printf("\n\n*** accuracy rate = %f, error rate = %f; time = %d (sec); %d docs\n", accrate, errrate, elapse, maxdoc);

		reader.close();
	}

	static Directory dir() throws IOException {
		return FSDirectory.open(new File(INDEX));
	}

	static void init() {
		counts = new int[CATEGORIES.length][CATEGORIES.length];
		catindex = new HashMap<String, Integer>();
		for (int i = 0; i < CATEGORIES.length; i++) {
			catindex.put(CATEGORIES[i], i);
		}
	}

	static int idx(String cat) {
		return catindex.get(cat);
	}
}